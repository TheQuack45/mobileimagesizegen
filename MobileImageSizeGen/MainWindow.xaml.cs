﻿using Microsoft.Win32;
using MobileImageSizeGen.Models;
using Newtonsoft.Json;
using Svg;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MobileImageSizeGen
{
    public class ColorItem
    {
        public SolidColorBrush Color { get; set; }
        public string Name { get; set; }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constants
        private static (int width, int height) LDPI_SIZE = (36, 36);
        private static (int width, int height) MDPI_SIZE = (48, 48);
        private static (int width, int height) HDPI_SIZE = (72, 72);
        private static (int width, int height) XHDPI_SIZE = (96, 96);
        private static (int width, int height) XXHDPI_SIZE = (144, 144);
        private static (int width, int height) XXXHDPI_SIZE = (192, 192);

        private static (int width, int height) IOS_BASE_SIZE = (60, 60);
        private static (int width, int height) IOS_SETTINGS_SIZE = (29, 29);
        private static (int width, int height) IOS_NOTIF_SIZE = (40, 40);
        private static (int width, int height) IOS_APP_STORE_SIZE = (1024, 1024);

        private const string NO_COLOR_CHANGE_TEXT = "No Change";

        private const string CONFIG_FILE_NAME = "Bundles.json";
        #endregion Constants

        public string OriginalName = null;

        private string _svgPath = null;
        private string _bundleDirectory = null;
        private List<Platform> _platforms;

        public MainWindow()
        {
            InitializeComponent();

            IEnumerable<ColorItem> colors = Enum.GetValues(typeof(KnownColor)).Cast<KnownColor>().Select(this.CreateColorItem);

            List<ColorItem> colorOptions = new List<ColorItem>();
            colorOptions.Add(new ColorItem() { Name = NO_COLOR_CHANGE_TEXT,
                                               Color = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 255, 255)), });
            colorOptions.AddRange(colors);

            this.ColorList.ItemsSource = colorOptions;
        }

        private ColorItem CreateColorItem(KnownColor knownColor)
        {
            System.Drawing.Color drawingColor = System.Drawing.Color.FromKnownColor(knownColor);
            System.Windows.Media.Color mediaColor = System.Windows.Media.Color.FromArgb(drawingColor.A,
                                                                                        drawingColor.R,
                                                                                        drawingColor.G,
                                                                                        drawingColor.B);

            return new ColorItem
            {
                Color = new SolidColorBrush(mediaColor),
                Name = drawingColor.Name,
            };
        }

        private void ChangeColor(IEnumerable<SvgElement> nodes, SvgColourServer color)
        {
            foreach (SvgElement node in nodes)
            {
                if (node.Fill != SvgPaintServer.None) node.Fill = color;
                if (node.Color != SvgPaintServer.None) node.Color = color;
                if (node.StopColor != SvgPaintServer.None) node.StopColor = color;
                if (node.Stroke != SvgPaintServer.None) node.Stroke = color;

                this.ChangeColor(node.Descendants(), color);
            }
        }

        private List<Platform> GetPlatforms(string configPath)
        {
            if (string.IsNullOrWhiteSpace(configPath))
            {
                throw new ArgumentException("The given config path is invalid.", nameof(configPath));
            }

            string jsonData;
            try
            {
                jsonData = File.ReadAllText(configPath);
            }
            catch (Exception)
            {
                return null;
            }

            PlatformCollection pc = JsonConvert.DeserializeObject<PlatformCollection>(jsonData);
            if (pc == null)
                { return null; }

            return pc.Platforms.ToList();

        }

        private void GenerateBundle(SvgDocument vector, string name)
        {
            var imageFormat = System.Drawing.Imaging.ImageFormat.Png;

            if (this._platforms == null)
            {
                return;
            }

            foreach (Platform platform in this._platforms)
            {
                var platformDirectory = Directory.CreateDirectory(System.IO.Path.Combine(this._bundleDirectory, platform.Name));

                foreach (Models.Size size in platform.Sizes)
                {
                    if (size.Name != null)
                    {
                        // Create a custom directory for this size with a specified name, then save the image with no size information in the name.
                        DirectoryInfo sizeDirectory = Directory.CreateDirectory(System.IO.Path.Combine(platformDirectory.FullName, size.Name));

                        vector.Draw(size.Width, size.Height).Save(System.IO.Path.Combine(sizeDirectory.FullName, $"{name}.png"), imageFormat);
                    }
                    else
                    {
                        // Save in the base directory with a name that specifies the size.
                        vector.Draw(size.Width, size.Height).Save(System.IO.Path.Combine(platformDirectory.FullName, $"{name}-{size.Width}x{size.Height}.png"), imageFormat);
                    }
                }
            }
        }

        private void SelectFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Vector Graphic (*.SVG)|*.svg;*.SVG";
            fileDialog.Multiselect = false;
            if (fileDialog.ShowDialog() == false)
            {
                this.GenerateButton.IsEnabled = false;
                return;
            }

            this._svgPath = fileDialog.FileName;
            this.OriginalName = fileDialog.SafeFileName;
            this.FileNameTextBox.Text = this.OriginalName;
            this.GenerateButton.IsEnabled = true;
        }

        private void GenerateButton_Click(object sender, RoutedEventArgs e)
        {
            if (this._svgPath == null)
            {
                this.GenerateButton.IsEnabled = false;
                return;
            }

            if (this.ColorList.SelectedItem == null)
            {
                this.GenerateButton.IsEnabled = false;
                return;
            }

            string imageName = !String.IsNullOrWhiteSpace(this.FileNameTextBox.Text) ? this.FileNameTextBox.Text : this.OriginalName;
            string imageNameWithExtenion = imageName.ToLower().EndsWith(".svg") ? imageName : imageName + ".svg";
            string imageNameNoExtension = imageNameWithExtenion.Remove(imageNameWithExtenion.Length - 4);

            this._bundleDirectory = System.IO.Directory.CreateDirectory($"./{imageNameNoExtension}").FullName;

            string baseFilePath = System.IO.Path.Combine(this._bundleDirectory, imageNameWithExtenion);
            System.IO.File.Copy(this._svgPath, baseFilePath, true);
            SvgDocument vectorFile = SvgDocument.Open(baseFilePath);

            ColorItem colorItem = this.ColorList.SelectedItem as ColorItem;
            if (colorItem.Name != NO_COLOR_CHANGE_TEXT)
            {
                System.Drawing.Color selectedColor = System.Drawing.Color.FromArgb(colorItem.Color.Color.A,
                                                                                   colorItem.Color.Color.R,
                                                                                   colorItem.Color.Color.G,
                                                                                   colorItem.Color.Color.B);
                this.ChangeColor(vectorFile.Descendants(), new SvgColourServer(selectedColor));
            }

            this.GenerateBundle(vectorFile, imageNameNoExtension);

            MessageBox.Show("Generation done!");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            List<Platform> platforms = this.GetPlatforms(CONFIG_FILE_NAME);

            if (platforms == null)
            {
                this.SelectFileButton.IsEnabled = false;
                this.GenerateButton.IsEnabled = false;
                this.ColorList.IsEnabled = false;
                this.FileNameTextBox.IsEnabled = false;

                MessageBox.Show("Failed to read the config file. Please ensure that it is valid and that you have permission to access it.");
            }
            else
            {
                this._platforms = platforms;
            }
        }
    }
}
