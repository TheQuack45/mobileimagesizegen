﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileImageSizeGen.Models
{
    public class Platform
    {
        public string Name { get; set; }
        public ICollection<Size> Sizes { get; set; }
    }
}
