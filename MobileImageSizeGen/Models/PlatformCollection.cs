﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobileImageSizeGen.Models
{
    public class PlatformCollection
    {
        public ICollection<Platform> Platforms { get; set; }
    }
}
